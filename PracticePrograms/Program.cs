﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePrograms
{
    class Program
    {
        static void Main(string[] args)
        {
            //GenericList<int> list = new GenericList<int>();

            //for(int i = 1; i<10;i++)
            //{
            //    list.AddHead(i);
            //}

            //foreach(int k in list)
            //{
            //    Console.Write(k + " ");
            //}

            //Console.WriteLine("\nDone");
            SortedList<string,string> st = new SortedList<string,string>();
            st.Add("ONe", "hi");

            ArrayList al = new ArrayList();
            al.Add(1);
            al.Add(true);
            al.Add('c');
            al.Add("F");
            al.Add(12.21);
            foreach(var item in al)
            {
                Console.WriteLine(item);
            }
        }
    }
}
